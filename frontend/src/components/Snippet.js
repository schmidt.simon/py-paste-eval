import React, { Component } from "react";
import { connect } from "react-redux";
import AceEditor from "react-ace";

import update from "immutability-helper";

import "brace/mode/python";
import "brace/theme/github";

import { Accordion, Grid, Label, Icon } from "semantic-ui-react";

import { load as loadSnippet, subscribe } from "../ducks/Snippet";
import _ from "lodash";

import SnippetResult from "./SnippetResult";

import "./Snippet.css";

class Snippet extends Component {
  state = { activeResults: {} };

  static defaultProps = {
    loading: false,
    failed: false
  };

  subscribeAndLoad = props => {
    // Load first to avoid weird races (maybe...)
    props.load(props.id).then(() => props.subscribe(props.id));
  };

  toggleResult = (e, { id }) =>
    this.setState(prevState =>
      update(prevState, {
        activeResults: {
          [id]: { $set: !this.resultIsActive(id) }
        }
      })
    );

  resultIsActive = id => _.get(this.state.activeResults, id, true);

  componentWillMount() {
    this.subscribeAndLoad(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.id !== this.props.id) {
      this.subcsribeAndLoad(nextProps);
    }
  }

  resultIcon = result => {
    if (result.status === "OK") {
      return result.returncode === 0 ? "checkmark" : "warning";
    }

    return {
      RUNNING: "wait",
      PENDING: "wait",
      FAILED: "x"
    }[result.status];
  };

  render() {
    if (this.props.failed) {
      return "Error :(";
    }

    const snippet = this.props.snippet;

    if (this.props.loading || snippet === undefined) {
      return "Loading...";
    }

    const results = _.values(snippet.results).map(result => [
      <Accordion.Title
        id={result.id}
        key={`title:${result.id}`}
        onClick={this.toggleResult}
        active={this.resultIsActive(result.id)}
      >
        <Label as="span">
          <Icon name={this.resultIcon(result)} />
          {result.environment}
        </Label>
      </Accordion.Title>,
      <Accordion.Content
        key={`content:${result.id}`}
        active={this.resultIsActive(result.id)}
      >
        <SnippetResult snippetId={this.props.id} resultId={result.id} />
      </Accordion.Content>
    ]);

    return (
      <Grid divided="vertically" className="snippet" columns={2}>
        <Grid.Row>
          <Grid.Column>
            <AceEditor
              mode="python"
              theme="github"
              value={snippet.code}
              readOnly={true}
            />
          </Grid.Column>

          <Grid.Column>
            <div className="results">
              <Accordion>{results}</Accordion>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  // Grab props from url parameters
  const id = _.get(ownProps, "match.params.snippet", ownProps.id);

  const snippet = state.snippet.data[id];

  return {
    id,
    snippet,
    loading: _.has(state.snippet.meta.loading, id),
    failed: _.has(state.snippet.meta.errors, id)
  };
};

const mapDispatchToProps = dispatch => ({
  load: id => dispatch(loadSnippet(id)),
  subscribe: id => dispatch(subscribe(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Snippet);
