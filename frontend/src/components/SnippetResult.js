import React, { Component } from "react";
import { connect } from "react-redux";
import { Loader } from "semantic-ui-react";

import { load as loadSnippet, subscribe } from "../ducks/Snippet";
import _ from "lodash";

import "./Snippet.css";

const loadData = props => {
  props.load(props.snippetId);
};

class SnippetResult extends Component {
  static defaultProps = {
    loading: false,
    failed: false
  };

  subscribeAndLoad = props => {
    // Subscribe first to avoid weird races (maybe...)
    props.subscribe(this.props.snippetId).then(
      () => loadData(this.props),
      () => loadData(this.props) // :(
    );
  };

  componentWillMount() {
    this.subscribeAndLoad(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.resultId !== this.props.resultId) {
      this.subcsribeAndLoad(nextProps);
    }
  }

  render() {
    if (this.props.failed) {
      return "Error :(";
    }

    const result = this.props.result;

    if (this.props.loading || result === undefined) {
      return "Loading...";
    }

    const showExit = result.returncode !== 0 && result.status === "OK";
    return (
      <div className="result">
        <code>
          <pre>{result.output}</pre>
          <Loader active={result.status === "RUNNING"} inline size="mini" />
        </code>
        {showExit ? <span>Exit code: {result.returncode}</span> : null}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  // Grab props from url parameters
  const id = _.get(ownProps, "match.params.snippet", ownProps.id);

  const snippet = state.snippet.data[ownProps.snippetId];
  const result = _.get(snippet, ["results", ownProps.resultId]);

  return {
    result,
    loading: _.has(state.snippet.meta.loading, id),
    failed: _.has(state.snippet.meta.errors, id)
  };
};

const mapDispatchToProps = dispatch => ({
  load: id => dispatch(loadSnippet(id)),
  subscribe: id => dispatch(subscribe(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(SnippetResult);
