import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";

import AceEditor from "react-ace";

import "brace/mode/python";
import "brace/theme/github";

import { create, redirect } from "../ducks/Snippet";
import { Form, Message, Checkbox } from "semantic-ui-react";
import uuid from "uuid/v4";

import "./CreateSnippet.css";

class CreateSnippet extends Component {
  state = { code: "", environments: ["python3.6"] };

  handleChange = (e, { name, value }) => this.setState({ [name]: value });

  onSubmit = data => {
    this.setState({ nonce: uuid() }, state => this.props.create(this.state));
  };

  handleCodeChange = value => {
    this.setState({ code: value });
  };

  handleEnvironmentChange = (e, { name, checked }) => {
    this.setState((prevState, props) => ({
      environments: checked
        ? _.union(prevState.environments, [name])
        : _.filter(prevState.environments, key => key !== name)
    }));
  };

  componentDidUpdate(prevProps, prevState) {
    const nonce = this.state.nonce;

    if (nonce === undefined || this.props.results[nonce] === undefined) {
      return;
    }

    const { result } = this.props.results[nonce];

    if (result) {
      this.props.onSuccess({ id: result, history: this.props.history });
    }
  }

  render() {
    const { nonce, environments } = this.state;
    let canSubmit = true;
    let error = false;

    if (nonce !== undefined && this.props.results[nonce]) {
      const { loading, result } = this.props.results[nonce];
      error = this.props.results[nonce];

      canSubmit = !(loading || result);
    }

    const environmentFields = ["python3.6", "python2.7"].map(environment => (
      <Form.Field key={environment}>
        <Checkbox
          name={environment}
          label={environment}
          checked={_.indexOf(environments, environment) !== -1}
          onChange={this.handleEnvironmentChange}
        />
      </Form.Field>
    ));

    return (
      <div className="create-snippet">
        <Form
          error={error !== false}
          onSubmit={canSubmit ? this.onSubmit : undefined}
        >
          <Form.Group inline>{environmentFields}</Form.Group>
          <AceEditor
            mode="python"
            theme="github"
            value={this.state.code}
            onChange={this.handleCodeChange}
          />
          <Form.Button>Submit</Form.Button>

          <Message error content={`${error}`} />
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    results: state.snippet.meta.create
  };
};

const mapDispatchToProps = dispatch => ({
  create: data => dispatch(create(data)),
  onSuccess: args => dispatch(redirect(args))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateSnippet);
