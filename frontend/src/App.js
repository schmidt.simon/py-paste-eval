import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { combineReducers, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

import "./App.css";

import snippetReducer from "./ducks/Snippet";

import Snippet from "./components/Snippet";
import CreateSnippet from "./components/CreateSnippet";

const App = () => (
  <Router>
    <div className="App">
      <ul className="nav-links">
        <li>
          <Link to="/create/">Create a snippet</Link>
        </li>
      </ul>

      <Route exact path="/create/" component={CreateSnippet} />
      <Route exact path="/snippet/:snippet/" component={Snippet} />
    </div>
  </Router>
);

const appReducer = combineReducers({
  snippet: snippetReducer
});

const store = applyMiddleware(thunk)(createStore)(appReducer);

export default () => (
  <Provider store={store}>
    <App />
  </Provider>
);
