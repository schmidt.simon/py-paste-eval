import { default as isomorphicFetch } from "isomorphic-fetch";

const API_HOST = process.env.REACT_APP_API_HOST || "127.0.0.1:8000";

const API_ROOT = `//${API_HOST}/api`;

function fetch(url, options) {
  return isomorphicFetch(url, options).then(response =>
    response.json().then(json => (response.ok ? json : Promise.reject(json)))
  );
}

function post(url, data) {
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  });
}

export async function snippetDetails(id) {
  return fetch(`${API_ROOT}/snippets/${id}/`);
}

export async function createSnippet(snippet) {
  return post(`${API_ROOT}/snippets/`, snippet);
}

const ws = window.location.protocol === "https:" ? "wss" : "ws";
export const snippetSocketURI = id => `${ws}://${API_HOST}/snippet/${id}/`;
