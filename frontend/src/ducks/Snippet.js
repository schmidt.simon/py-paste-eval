import update from "immutability-helper";
import _ from "lodash";

import * as utils from "./utils";
import {
  snippetDetails as apiDetails,
  createSnippet as apiCreate,
  snippetSocketURI
} from "../api";

const initialState = {
  meta: { loading: {}, errors: {}, subscriptions: [], create: {} },
  data: {}
};

function messageReducer(state, action) {
  switch (action.data.type) {
    case "result":
      return update(state, {
        data: {
          [action.id]: {
            results: {
              [action.data.result.id]: { $set: action.data.result }
            }
          }
        }
      });
    case "result-chunk":
      const oldResult = _.get(state, [
        "data",
        action.id,
        "results",
        action.data.result_id
      ]);

      if (oldResult === undefined) {
        console.info("Got partial result but have no previous one", action);
        return state;
      }
      if (oldResult.status !== "RUNNING") {
        // TODO or no old result, deal with it

        // already have the complete version
        return state;
      }

      const lastIndex = oldResult.index === undefined ? -1 : oldResult.index;

      if (action.data.index - 1 !== lastIndex) {
        // Out of sync, inform?
        return state;
      }

      const oldOutput = oldResult.output === null ? "" : oldResult.output;
      const output = `${oldOutput}${action.data.chunk}`;
      return update(state, {
        data: {
          [action.id]: {
            results: {
              [action.data.result_id]: {
                index: { $set: action.data.index },
                output: { $set: output }
              }
            }
          }
        }
      });

    default:
      console.info(`Unknown snippet message type: ${action.type}`);
      return state;
  }
}

export default function reducer(incomingState = initialState, action) {
  let state = incomingState;

  switch (action.type) {
    case "snippet.LOAD":
      return utils.updateStateOnLoad(state, action.id);

    case "snippet.LOADED":
      return utils.updateStateOnLoaded(state, action.id, action.data);

    case "snippet.ERROR":
      return utils.updateStateOnError(state, action.id, action.error);

    case "snippet.CONNECT":
      return utils.updateStateOnSubscribe(state, action.id);

    // case 'snippet.OPEN'

    case "snippet.CLOSE":
      return utils.updateStateOnSubscribeClose(state, action.id);

    case "snippet.SUBSCRIBE_ERROR":
      return utils.updateStateOnSubscribeError(state, action.id);

    case "snippet.MESSAGE":
      return messageReducer(state, action);

    case "snippet.CREATE":
      return utils.updateStateOnCreate(state, action.nonce);

    case "snippet.ERROR_CREATE":
      return utils.updateStateOnCreateError(state, action.nonce, action.error);

    case "snippet.CREATED":
      return utils.updateStateOnCreated(state, action.nonce, action.data);

    default:
      return state;
  }
}

const onLoad = id => ({ type: "snippet.LOAD", id });
const onLoaded = (id, data) => ({ type: "snippet.LOADED", id, data });
const onError = (id, error) => ({ type: "snippet.ERROR", id, error });

const onCreate = nonce => ({ type: "snippet.CREATE", nonce });
const onCreated = (nonce, data) => ({ type: "snippet.CREATED", nonce, data });
const onCreateError = (nonce, error) => ({
  type: "snippet.ERROR_CREATE",
  nonce,
  error
});

const onConnect = (id, socket) => ({ type: "snippet.CONNECT", id, socket });
const onOpen = id => ({ type: "snippet.OPEN", id });
const onClose = id => ({ type: "snippet.CLOSE", id });
const onSubscribeError = id => ({ type: "snippet.SUBSCRIBE_ERROR", id });
const onMessage = (id, data) => ({ type: "snippet.MESSAGE", id, data });

export const load = id => async (dispatch, getState) => {
  const { snippet: { meta: { loading, errors }, data } } = getState();

  if (data[id] || loading[id] || errors[id]) {
    return;
  }

  dispatch(onLoad(id));
  try {
    const response = await apiDetails(id);
    dispatch(onLoaded(id, response));
  } catch (err) {
    dispatch(onError(id, err));
  }
};

export const create = ({ code, environments, nonce }) => async (
  dispatch,
  getState
) => {
  dispatch(onCreate(nonce));
  try {
    const response = await apiCreate({ code, environments });
    dispatch(onCreated(nonce, response));
    dispatch(subscribe(response.id));
  } catch (error) {
    dispatch(onCreateError(nonce, error));
  }
};

export const redirect = ({ history, id }) => async (dispatch, getState) => {
  history.push(`/snippet/${id}/`);
};

export const subscribe = id => async (dispatch, getState) => {
  const state = getState().snippet;
  utils.subscribe({
    dispatch,
    state,
    id,
    uri: snippetSocketURI(id),
    onConnect,
    onOpen,
    onClose,
    onError: onSubscribeError,
    onMessage
  });
};
