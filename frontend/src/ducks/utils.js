import update from "immutability-helper";
import _ from "lodash";

export const valuesFromState = (state, ids) => {
  const errors = _.pick(state.meta.errors, ids);
  const loading = _.pick(state.meta.loading, ids);
  const data = _.pick(state.data, ids);
  const missing = _.difference(
    ids.map(id => "" + id),
    _.keys(errors),
    _.keys(loading),
    _.keys(data)
  );

  return { errors, loading, missing, data };
};

// Example:
// objectMap([1,2,3], (i) => {key: i, value: 2*i}) == {1: 1, 2: 4, 3: 6}
//
// f should return {key, value} which will be used
// to create an object
export const objectMap = (collection, f) =>
  _.reduce(
    collection,
    function(result, x) {
      const { key, value } = f(x);
      result[key] = value;
      return result;
    },
    {}
  );

export const updateStateOnLoad = (state, id) =>
  update(state, { meta: { loading: { [id]: { $set: true } } } });

export const updateStateOnLoaded = (state, id, data) =>
  update(state, {
    meta: {
      loading: { $unset: [id] },
      errors: { $unset: [id] }
    },
    data: { [id]: { $set: data } }
  });

export const updateStateOnListLoaded = (state, data) =>
  update(state, {
    meta: {
      loading: { $unset: [data.map(x => x.id)] },
      errors: { $unset: [data.map(x => x.id)] }
    },
    data: objectMap(data, x => ({
      key: x.id,
      value: { $set: x }
    }))
  });

export const updateStateOnError = (state, id, error) =>
  update(state, {
    meta: {
      loading: { $unset: [id] },
      errors: { [id]: { $set: error } }
    }
  });

export const updateStateOnCreate = (state, nonce) =>
  update(state, {
    meta: {
      create: { [nonce]: { $set: { loading: true, error: false } } }
    }
  });

export const updateStateOnCreateError = (state, nonce, error) =>
  update(state, {
    meta: {
      create: { [nonce]: { $set: { loading: false, error: error } } }
    }
  });

export const updateStateOnCreated = (state, nonce, data) =>
  updateStateOnLoaded(
    update(state, {
      meta: {
        create: {
          [nonce]: { $set: { loading: false, error: false, result: data.id } }
        }
      }
    }),
    data
  );

export const updateStateOnSubscribe = (state, id) =>
  update(state, {
    meta: {
      subscriptions: { [id]: { $set: { active: true, error: false } } }
    }
  });

export const updateStateOnSubscribeClose = (state, id) =>
  update(state, {
    meta: {
      subscriptions: { $unset: [id] }
    }
  });

export const updateStateOnSubscribeError = (state, id) =>
  update(state, {
    meta: {
      subscriptions: { [id]: { $set: { ative: false, error: true } } }
    }
  });

export const isSubscriptionActive = (state, id) =>
  _.get(state, ["meta", "subscriptions", id, "active"], false);

export const subscribe = ({
  dispatch,
  state,
  id,
  uri,
  onConnect,
  onOpen,
  onError,
  onMessage,
  onClose
}) => {
  if (!("WebSocket" in window)) {
    console.error("No WebSocket in window, old browser?");
    return;
  }

  if (isSubscriptionActive(state, id)) {
    // Already subscribed
    return;
  }

  const socket = new WebSocket(uri);
  dispatch(onConnect(id, socket));

  socket.onopen = () => dispatch(onOpen(id, socket));
  socket.onerror = () => dispatch(onError(id, socket));
  socket.onmessage = evt => {
    if (evt.type === "message") {
      const data = JSON.parse(evt.data);
      dispatch(onMessage(id, data));
    }
  };
  socket.onclose = () => dispatch(onClose(id));
};
