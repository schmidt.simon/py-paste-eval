
#### Development

```
docker-compose -f docker-compose.dev.yml up
```

You'll get:

* [frontend](http://127.0.0.1:3000/)
* [flower](http://127.0.0.1:5555/)


#### Configuration

All goes in environment (or .env)


```
# dev or prod
ENVIRONMENT=dev
DJANGO_SECRET_KEY=hunter2
ALLOWED_HOSTS=127.0.0.1,api.localdomain
DATABASE_URL=postgres://user:password@postgres/backend

# Some duplication...
POSTGRES_USER=user
POSTGRES_PASSWORD=password

#REDIS_URL=redis://redis:6379

# Where frontend does the api requests
#REACT_APP_API_HOST=api.example.com


#CORS_ORIGIN_WHITELIST=paste.example.com

# Domains routed by nginx-proxy
#DOMAIN_API=api.example.com
#DOMAIN_WWW=paste.example.com

#LETSENCRYPT_EMAIL=

# Set to use letsencrypt test ca
#LETSENCRYPT_TEST=true


# Python junk
PYTHONUNBUFFERED=1
```


#### shitty deploy

```
export LC_ALL=en_US.UTF-8

dnf -y install docker docker-compose
systemctl enable --now docker

setsebool httpd_can_network_connect 1 -P

# Copy over the docker-compose.prod.yml (save it as docker-compose.yml)
...

# Setup env variables
cat > .env


# Migrations
docker-compose run backend-daphne python manage.py migrate

# Run it:
docker-compose up --scale backend-worker=4 --scale backend-celery=4

# To update docker-compose pull, run migrations, restart
```
