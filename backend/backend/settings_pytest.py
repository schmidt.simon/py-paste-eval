# isort:skip_file

import os

os.environ['ENVIRONMENT'] = 'test'
os.environ['DJANGO_SECRET_KEY'] = 'test'
os.environ['DATABASE_URL'] = 'sqlite://'


from .settings import *  # noqa


SNIPPET_EVALUATOR = {
    'evaluator': {
        'backend': 'paste_eval.evaluators.EchoEvaluator',
        'config': {},
        'environments': {
            'echo': {}
        },
    },
    'aggregator': {
        'backend': 'paste_eval.evaluators.MemoryChunkAggregator',
        'config': {},
    },
    'timeout': 30,
}
