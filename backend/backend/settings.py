"""
Django settings for backend project.

Generated by 'django-admin startproject' using Django 1.11.6.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
from os.path import dirname, join

import dj_database_url
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '..', '..', '.env')
load_dotenv(dotenv_path)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


if os.environ['ENVIRONMENT'] == 'prod':
    DEBUG = False
    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_BROWSER_XSS_FILTER = True
    CSRF_COOKIE_SECURE = True
    X_FRAME_OPTIONS = 'DENY'

    # Dummy access just to throw an error when unset
    os.environ['DJANGO_SECRET_KEY']
else:
    DEBUG = True


REDIS_URL = os.environ.get('REDIS_URL', 'redis://redis:6379')


SNIPPET_EVALUATOR = {
    'evaluator': {
        'backend': 'paste_eval.evaluators.DockerEvaluator',
        'config': {
            # Options that will always be passed to evaluator on initialization
            'url': 'docker+unix:///var/run/docker.sock',
        },
        'environments': {
            # Extra options that will be provided depending on the evaluation
            # environment.
            'python3.6': {
                'docker_image': 'python:3.6-alpine3.6',
            },
            'python2.7': {
                'docker_image': 'python:2.7-alpine3.6',
            },
        },
    },
    'aggregator': {
        'backend': 'paste_eval.evaluators.RedisChunkAggregator',
        'config': {
            'redis_url': REDIS_URL,
            'expire': 120,
        }
    },
    'timeout': 30,
}


# STATIC_PATH = os.environ.get('STATIC_PATH', 'static')
SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY', 'dummy')

if 'ALLOWED_HOSTS' in os.environ:
    ALLOWED_HOSTS = list(os.environ['ALLOWED_HOSTS'].split(','))
else:
    ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'channels',

    # TODO Something weird with this
    # 'corsheaders',

    'backend',
    'paste_eval.apps.PasteEvalConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'backend.urls'

CORS_URLS_REGEX = r'^/api/.*$'

CORS_ORIGIN_WHITELIST = os.environ.get('CORS_ORIGIN_WHITELIST', '127.0.0.1:3000').split(',')


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'backend.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': dj_database_url.config(conn_max_age=600),
}


CELERY_BROKER_URL = REDIS_URL + '/0'

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "ROUTING": "paste_eval.routing.channel_routing",
        "CONFIG": {
            "hosts": [REDIS_URL + '/1'],
        },
    },
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'


_loglevel = 'DEBUG' if DEBUG else 'INFO'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(name)s %(pathname)s/%(filename)s:%(lineno)d  %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'paste_eval': {
            'handlers': ['console'],
            'level': _loglevel,
        },
    },
}
