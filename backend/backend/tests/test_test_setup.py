import os


def test_environment():
    assert os.environ['DJANGO_SETTINGS_MODULE'] == 'backend.settings_pytest'
    assert os.environ['ENVIRONMENT'] == 'test'
