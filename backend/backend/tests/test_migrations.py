import pytest
from django.core.management import call_command


@pytest.mark.django_db
def test_missing_migrations():
    try:
        call_command('makemigrations', check=True, dry_run=True)
    except SystemExit:
        assert False, "There are missing migrations"
