from django.conf.urls import include, url
from django.contrib import admin
from django.http import HttpResponse


def status(request):
    return HttpResponse('OK')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^status/', status),
    url(r'^', include('paste_eval.urls')),
]
