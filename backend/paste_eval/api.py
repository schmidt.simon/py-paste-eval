from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from .models import Snippet
from .serializers import SnippetSerializer

__all__ = ['SnippetViewSet']


class SnippetViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     GenericViewSet):
    queryset = Snippet.objects.prefetch_related('results').all()
    serializer_class = SnippetSerializer
