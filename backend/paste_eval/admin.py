from django.contrib import admin

from .models import Result, Snippet

admin.site.register(Snippet)
admin.site.register(Result)
