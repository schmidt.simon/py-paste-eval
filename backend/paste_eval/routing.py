from channels.routing import route

from .consumers import ws_connect, ws_disconnect

channel_routing = [
    route("websocket.connect", ws_connect, path=r"^/snippet/(?P<snippet_id>[^/]{,200})/$"),
    route("websocket.disconnect", ws_disconnect, path=r"^/snippet/(?P<snippet_id>[^/]{,200})/$"),
]
