from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .api import SnippetViewSet

router = DefaultRouter()
router.register(r'snippets', SnippetViewSet)

urlpatterns = [
    url(r'^api/', include(router.urls)),
]
