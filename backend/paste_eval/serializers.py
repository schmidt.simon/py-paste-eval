from rest_framework import serializers

from .models import Result, Snippet


def CodeField(**kwargs):
    return serializers.CharField(trim_whitespace=False, allow_blank=True)


class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ('id', 'snippet_id', 'environment', 'output', 'returncode', 'status')

    status = serializers.SerializerMethodField()
    output = CodeField()

    def get_status(self, obj):
        return obj.status.name


class SnippetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Snippet
        read_only_fields = ('id', 'results', 'returncode')
        fields = ('id', 'results', 'code', 'environments')

    results = serializers.SerializerMethodField()
    code = CodeField(required=True, null=False)

    environments = serializers.ListField(
        child=serializers.CharField(max_length=100, required=True),
        required=False,
        default=['python3.6'],
        write_only=True)

    def get_results(self, snippet):
        results = ResultSerializer(snippet.results, many=True)
        return {result['id']: result for result in results.data}

    def create(self, validated_data):
        # Same code might exist already
        id = Snippet.code_to_id(validated_data['code'])

        instance, create_ = Snippet.objects.get_or_create(
            id=id,
            defaults={'code': validated_data['code']})

        for environment in validated_data['environments']:
            Result.objects.get_or_create(
                snippet_id=instance.pk,
                environment=environment,
                defaults={'_status': Result.STATUS.PENDING.value}
            )

        return instance


class ResultChunkSerializer(serializers.Serializer):
    class Meta:
        fields = ('type', 'chunk', 'index', 'result_id', 'status')

    type = serializers.CharField(default='result-chunk')
    chunk = CodeField(required=True, allow_blank=True)
    index = serializers.IntegerField(required=True)
    result_id = serializers.UUIDField(required=True)

    def validate_type(self, value):
        if value != 'result-chunk':
            raise serializers.ValidationError("Must be 'result-chunk'")

        return value
