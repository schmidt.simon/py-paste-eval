import base64
import enum
import uuid
from hashlib import blake2b

from django.db import models


class Snippet(models.Model):
    id = models.CharField(
        default=None,
        max_length=128,
        null=False,
        blank=True,
        primary_key=True,
        editable=False)

    # ffs... 100character limit in group names
    channels_group_name = models.UUIDField(default=uuid.uuid4, blank=False, null=False)

    # TODO store somewhere else
    code = models.TextField(max_length=4096, null=False, blank=False)

    @staticmethod
    def code_to_id(code: str):
        return base64.urlsafe_b64encode(
            blake2b(code.encode('utf8'), digest_size=32).digest()
        ).decode('utf8')

    def save(self, *args, **kwargs):
        code_id = self.code_to_id(self.code)

        if self.id is None:
            self.id = code_id
        elif self.id != code_id:
            raise ValueError(f"id/code mismatch! id: {self.id} hash: {code_id}")

        return super().save(*args, **kwargs)


class Result(models.Model):
    id = models.UUIDField(default=uuid.uuid4, null=False, primary_key=True)
    snippet = models.ForeignKey(
        Snippet,
        null=False,
        blank=False,
        related_name='results',
        on_delete=models.CASCADE)

    environment = models.CharField(null=False, blank=False, default=None, max_length=100)
    output = models.TextField(max_length=4096, null=True, blank=True, default=None)
    returncode = models.IntegerField(default=None, null=True, blank=True, db_index=True)

    @enum.unique
    class STATUS(enum.Enum):
        PENDING = 3
        RUNNING = 0
        OK = 1
        FAILED = 2

    _status = models.IntegerField(
        choices=[(s.value, s.name) for s in STATUS],
        null=False,
        blank=False)

    @property
    def status(self):
        return self.STATUS(self._status)

    @status.setter
    def status(self, value):
        self._status = value.value
