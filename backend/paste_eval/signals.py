import logging

from django.db import transaction
from django.db.models.signals import post_save
from django.dispatch import Signal, receiver

from . import notifications, tasks
from .models import Result

__all__ = ['result_saved', 'on_partial_result', 'partial_result']

logger = logging.getLogger('%s' % __name__)

partial_result = Signal(providing_args=['result_id', 'chunk', 'index'])


@receiver(post_save, sender=Result)
def result_saved(sender, *, instance, raw, **kwargs):
    if raw:
        return

    result = instance

    if result.status == Result.STATUS.PENDING:
        logger.debug("result_saved, scheduling evaluation. result: %s", result.pk)
        transaction.on_commit(
            lambda: tasks.evaluate_snippet.delay(
                snippet_id=result.snippet.pk,
                result_id=f'{result.id}')
        )

    elif result.status in (Result.STATUS.OK, Result.STATUS.FAILED):
        logger.debug("result_saved, notifying completion: %s", result.pk)
        transaction.on_commit(lambda: notifications.notify_snippet_result(result))


@receiver(partial_result)
def on_partial_result(*, result_id, chunk, index, **kwargs):
    logger.debug("on_partial_result, result_id: %s, index: %s", result_id, index)

    group_name = Result.objects.values_list('snippet__channels_group_name', flat=True).get(id=result_id)
    notifications.notify_snippet_partial_result(
        channel=f'{group_name}',
        result_id=result_id,
        chunk=chunk,
        index=index)
