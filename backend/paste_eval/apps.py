from django.apps import AppConfig


class PasteEvalConfig(AppConfig):
    name = 'paste_eval'

    def ready(self):
        from . import tasks  # noqa
        from . import signals  # noqa
