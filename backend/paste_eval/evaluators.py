import collections
import itertools
from typing import Callable, Iterable, Optional

import attr
import docker
import redis
import requests
import requests.exceptions
from django.conf import settings
from rest_framework.settings import import_from_string

from .models import Result, Snippet
from .signals import partial_result


class Evaluator:
    """
    Type for evaluators
    """
    def evaluate(self) -> Iterable[str]:
        """
        Yield chunk after chunk, and finaly raise `StopIteration(returncode)`
        """
        raise NotImplementedError()


class ChunkAggregator:
    """
    Type for aggregators of partial evaluation output

    init is given keyword argument `identifier: str`
    """
    def handle_chunk(self, chunk: str):
        raise NotImplementedError()

    @property
    def history(self) -> Iterable[str]:
        """
        All chunks so far

        Needed so clients can catch up if they join the stream half-way
        """
        raise NotImplementedError()

    def cleanup(self):
        """
        Called when result is not needed anymore
        """
        raise NotImplementedError()


@attr.s
class DockerEvaluator(Evaluator):
    url = attr.ib()
    docker_image = attr.ib()

    @property
    def docker_url(self):
        assert self.url.startswith('docker+')
        return self.url[len('docker+'):]

    def evaluate(self, code):
        client = docker.DockerClient(base_url=self.docker_url, version='1.32')

        try:
            container = client.containers.run(
                self.docker_image,
                command=['python', '-c', code],
                environment={'PYTHONUNBUFFERED': '1'},
                stdout=True,
                stderr=True,
                network_disabled=True,
                mem_limit='50m',
                memswap_limit='100m',
                pids_limit=1,
                read_only=True,
                detach=True,
            )
            output_stream = container.logs(stdout=True, stderr=True, stream=True)

            # Should group by lines or similar
            # also it's not possible to decode a utf8 chunk since a multibyte might
            # have been split up, TODO FIXME, probably involves using bytes everywhere
            # which might be a pain in the chunk passing, base64 there perhaps.
            yield from (
                chunk.decode('utf8')
                for chunk in output_stream)

            # By now it is dead (I think)
            # but must refresh the state so ExitCode is populated properly
            container.reload()
            return container.attrs['State']['ExitCode']
        except requests.exceptions.ReadTimeout:
            # deal with it!
            raise
        except UnicodeDecodeError:
            # deal with it!
            raise
        finally:
            container.remove(v=True, force=True)


@attr.s
class EchoEvaluator(Evaluator):
    """Silly evaluator for testing"""
    def evaluate(self, code):
        for line in code.splitlines(True):
            yield line
        return 0


@attr.s
class RedisChunkAggregator(ChunkAggregator):
    identifier = attr.ib()
    redis_url = attr.ib()
    expire = attr.ib(default=None)

    _expiration_set = False

    @property
    def connection(self):
        return redis.from_url(self.redis_url)

    @property
    def key(self):
        return f'chunk_agg:{self.identifier}'

    @property
    def history(self):
        return (v.decode('utf8') for v in self.connection.lrange(self.key, 0, -1))

    def handle_chunk(self, chunk: str):
        self.connection.rpush(self.key, chunk)

        if self.expire is not None and not self._expiration_set:
            self.connection.expire(self.key, self.expire)
            self._expiration_set = True

    def cleanup(self):
        self.connection.delete(self.key)


@attr.s
class MemoryChunkAggregator(ChunkAggregator):
    """Only suitable for testing"""
    identifier = attr.ib()

    _memory = collections.defaultdict(list)

    @property
    def chunks(self):
        return self._memory[self.identifier]

    def handle_chunk(self, chunk: str):
        self.chunks.append(chunk)

    @property
    def history(self):
        return self.chunks

    def cleanup(self):
        self._memory[self.identifier] = "CLEANED"


def evaluate(
        *,
        chunk_aggregator,
        evaluator,
        on_chunk: Optional[Callable[[str, int], None]] = None,
        code,
        ) -> dict:
    chunks = evaluator.evaluate(code)

    returncode = -1

    index = 0
    while True:
        try:
            chunk = next(chunks)
        except StopIteration as stop:
            returncode = stop.value
            break

        chunk_aggregator.handle_chunk(chunk)

        if on_chunk is not None:
            on_chunk(chunk, index)

        index += 1

    return {'returncode': returncode, 'output': ''.join(chunk_aggregator.history)}


def default_evaluator(environment):
    conf = settings.SNIPPET_EVALUATOR['evaluator']
    backend = import_from_string(conf['backend'], 'SNIPPET_EVALUATOR.evaluator.backend')

    evaluator_config = {
        key: value
        for key, value in itertools.chain(
            conf['config'].items(),
            conf['environments'][environment].items()
        )
    }

    return backend(**evaluator_config)


def default_aggregator(identifier):
    conf = settings.SNIPPET_EVALUATOR['aggregator']

    backend = import_from_string(conf['backend'], 'SNIPPET_EVALUATOR.aggregator.backend')
    return backend(**conf['config'], identifier=identifier)


def evaluate_snippet(snippet_id, result):
    code, group_name = Snippet.objects.values_list(
            'code', 'channels_group_name',
        ).get(id=snippet_id)
    group_name = str(group_name)

    result_id = f'{result.id}'

    aggregator = default_aggregator(identifier=result_id)
    evaluator = default_evaluator(result.environment)

    try:
        evaluation_result = evaluate(
            chunk_aggregator=aggregator,
            evaluator=evaluator,
            code=code,
            on_chunk=lambda chunk, index: partial_result.send(
                None,
                result_id=result_id,
                chunk=chunk,
                index=index),
            )

        result.output = evaluation_result['output']
        result.returncode = evaluation_result['returncode']
        result.status = Result.STATUS.OK
        result.save()
    finally:
        aggregator.cleanup()
