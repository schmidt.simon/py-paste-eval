from channels import Group

from .evaluators import default_aggregator
from .models import Result, Snippet
from .notifications import notify_snippet_partial_result, notify_snippet_result


def _catch_up_with_results(result_ids, channel):
    for result_id in result_ids:
        aggregator = default_aggregator(result_id)
        for ix, chunk in enumerate(aggregator.history):
            notify_snippet_partial_result(channel, result_id, chunk, ix)


def ws_connect(message, snippet_id):
    group = Snippet.objects.values_list('channels_group_name', flat=True).get(id=snippet_id)
    message.reply_channel.send({"accept": True})
    Group(f"{group}").add(message.reply_channel)

    # Catch up to partial evaluation results
    result_ids = (Result.objects
                        .filter(snippet_id=snippet_id, _status=Result.STATUS.RUNNING.value)
                        .values_list('id', flat=True))

    _catch_up_with_results(result_ids, message.reply_channel)

    # TODO better solution
    # Race condition:
    # * POST create new snippet
    #    user: Gets snippet info (id in particular)
    #    server: starts and completes evaluation
    #    user: opens socket for snippet id but has already missed the partial results and the new result signal
    for result in Result.objects.filter(snippet_id=snippet_id).exclude(id__in=result_ids):
        notify_snippet_result(result, channel=message.reply_channel)


def ws_disconnect(message, snippet_id):
    group = Snippet.objects.values_list('channels_group_name', flat=True).get(id=snippet_id)
    Group(f"{group}").discard(message.reply_channel)
