from channels import Group
from rest_framework.renderers import JSONRenderer

from .serializers import ResultChunkSerializer, ResultSerializer


def notify_snippet_result(result, channel=None):
    if channel is None:
        channel = Group(f'{result.snippet.channels_group_name}')

    data = {"type": "result", "result": ResultSerializer(result).data}

    channel.send({
        "text": JSONRenderer().render(data).decode('utf8')
    })


def notify_snippet_partial_result(channel, result_id, chunk, index):
    if isinstance(channel, str):
        channel = Group(channel)

    data = {
            "type": "result-chunk",
            "chunk": chunk,
            "index": index,
            "result_id": result_id
        }

    serializer = ResultChunkSerializer(data=data)
    serializer.is_valid(raise_exception=True)

    data = serializer.validated_data

    channel.send({
        "text": JSONRenderer().render(data).decode('utf8')
    })
