import json
import uuid
from unittest.mock import Mock

import pytest

from ..consumers import _catch_up_with_results
from ..evaluators import default_aggregator
from ..models import Result
from ..notifications import notify_snippet_partial_result


@pytest.mark.django_db
def test_notify_snippet_partial_result():

    chunks = ['', ' ', '\n', 'hello', '☃']

    for index, chunk in enumerate(chunks):
        channel = Mock()

        notify_snippet_partial_result(
            channel=channel,
            result_id=uuid.uuid4(),
            chunk=chunk,
            index=index)
        channel.send.assert_called_once()

        message = channel.send.call_args[0][0]['text']
        data = json.loads(message)

        assert data['type'] == 'result-chunk'
        assert data['chunk'] == chunk
        assert data['index'] == index


@pytest.mark.django_db
def test_result_catch_up(snippet):
    # TODO factories
    result = Result(snippet_id=snippet.id,
                    _status=Result.STATUS.RUNNING.value,
                    environment='echo')
    result.save()
    result_id = f'{result.id}'

    channel = Mock()

    _catch_up_with_results([result_id], channel=channel)
    channel.send.assert_not_called()

    aggregator = default_aggregator(result_id)
    aggregator.handle_chunk("Hello!")

    _catch_up_with_results([result_id], channel=channel)
    channel.send.assert_called_once()
