import pytest

from ..models import Snippet


@pytest.fixture(scope='function')
def snippet():
    snippet = Snippet(code="Hello world")
    snippet.save()
    return snippet
