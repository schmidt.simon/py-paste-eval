import pytest

from .. import notifications
from ..evaluators import EchoEvaluator, MemoryChunkAggregator, default_aggregator, evaluate, evaluate_snippet
from ..models import Result, Snippet


def test_evaluate():
    evaluator = EchoEvaluator()
    chunk_aggregator = MemoryChunkAggregator(identifier='hello')

    evaluation = evaluate(
        chunk_aggregator=chunk_aggregator,
        evaluator=evaluator,
        code="Hello World!")

    assert evaluation['output'] == 'Hello World!'
    assert evaluation['returncode'] == 0


@pytest.mark.django_db
def test_snippet_evaluation(monkeypatch):

    partial_notify_triggered = False

    def monkey_notify(*args, **kwargs):
        nonlocal partial_notify_triggered
        partial_notify_triggered = True

    monkeypatch.setattr(notifications, 'notify_snippet_partial_result', monkey_notify)

    snippet = Snippet(code="a\nb\nc")
    snippet.save()

    result = Result.objects.create(snippet_id=snippet.id,
                                   environment='echo',
                                   _status=Result.STATUS.RUNNING.value)

    evaluate_snippet(snippet.id, result)

    assert default_aggregator(identifier=f'{result.id}').history == 'CLEANED'
    assert result.output == snippet.code
    assert result.status == Result.STATUS.OK
    assert partial_notify_triggered
