from celery import shared_task
from celery.exceptions import SoftTimeLimitExceeded
from django.conf import settings
from django.db import transaction

from . import evaluators
from .models import Result


@shared_task(
    soft_time_limit=settings.SNIPPET_EVALUATOR['timeout'],
)
def evaluate_snippet(snippet_id: str, result_id: str):
    with transaction.atomic():
        result = Result.objects.get(id=result_id)

        if result.status == Result.STATUS.RUNNING:
            raise RuntimeError("Work on this result has already started")

        result.status = Result.STATUS.RUNNING
        result.save()

    try:
        evaluators.evaluate_snippet(snippet_id, result)
    except SoftTimeLimitExceeded:
        # No need to raise this one
        Result.objects.filter(id=result_id).update(_status=Result.STATUS.FAILED.value)
    except Exception:
        Result.objects.filter(id=result_id).update(_status=Result.STATUS.FAILED.value)
        raise
